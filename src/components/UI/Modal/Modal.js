import React from 'react'

import Backdrop from '../Backdrop/Backdrop'

import classes from './Modal.css'
import Aox from '../../../hoc/Aox'

const Modal = (props) => (
  <Aox>
    <Backdrop
      show={props.show}
      clicked={props.modalClosed}
    />
    <div
      className={classes.Modal}
      style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'
      }}>

      {props.children}
    </div>
  </Aox>
)

export default Modal
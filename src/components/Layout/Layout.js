import React from 'react'

import Aox from '../../hoc/Aox'
import classes from './Layout.css'

const Layout = (props) => (
  <Aox>
    <div>Toolar, SideDrawer, Backdrop</div>
    <main className={classes.Content}>
      {props.children}
    </main>
  </Aox>
)

export default Layout